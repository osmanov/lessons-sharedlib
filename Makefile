# Copyright © 2015 - Ruslan Osmanov <rrosmanov@gmail.com> 

# Program for compiling C programs; default ‘cc’.
CC=gcc

# Extra flags to give to the C preprocessor and programs that use it (the C and
# Fortran compilers).
CPPFLAGS=-I./include

# CFLAGS - compilation flags
#-march=native: this tells GCC to automatically detect the CPU architecture to
#               use instead of manually specifying one
#-O2:           this tells GCC to optimize the code it produces
#
#-pipe:         this speeds up the compilation process in that temporary files are not
#               used for the various compilation stages
#
#-fPIC          Position-Independent Code: the functions in the library may be
#               loaded at different addresses in different programs.
CFLAGS=-Wall -g $(CPPFLAGS)

# Every shared library has a special name called the ``soname''. The soname has
# the prefix ``lib'', the name of the library, the phrase ``.so'', followed by
# a period and a version number that is incremented whenever the interface
# changes.
LIBEXAMPLE_SONAME=libexample.so.1

# Every shared library also has a ``real name'', which is the filename
# containing the actual library code. The real name adds to the soname a
# period, a minor number, another period, and the release number. The last
# period and release number are optional.
LIBEXAMPLE_REALNAME=libexample.so.1.0

# Installation directory for `ldconfig`
LIBEXAMPLE_INSTALL_DIR=$(shell pwd)

RESULT_TARGETS=libexample.so libexample.a test test-static

all: $(RESULT_TARGETS)

example.o: example.c
	$(CC) $(CFLAGS) -fPIC -c -o example.o example.c

libexample.so: example.o
	$(CC) $(CFLAGS) -shared -Wl,-soname,$(LIBEXAMPLE_SONAME) \
		-o $(LIBEXAMPLE_REALNAME) example.o
	ldconfig -n .
	ln -sf $(LIBEXAMPLE_SONAME) libexample.so

test: libexample.so test.c
	$(CC) $(CFLAGS) -o test test.c -L. -lexample

test-static: libexample.a test.c
	$(CC) $(CFLAGS) -static -o test-static test.c -L. -lexample

install-libexample:
	mkdir -p $(LIBEXAMPLE_INSTALL_DIR)/lib $(LIBEXAMPLE_INSTALL_DIR)/include
	cp $(LIBEXAMPLE_REALNAME) $(LIBEXAMPLE_INSTALL_DIR)/lib
	ldconfig -n $(LIBEXAMPLE_INSTALL_DIR)/lib
	ln -sf $(LIBEXAMPLE_SONAME) $(LIBEXAMPLE_INSTALL_DIR)/lib/libexample.so

libexample.a: install-libexample
	ar cr libexample.a example.o

install: $(RESULT_TARGETS)
	[ './include' -ef "$(LIBEXAMPLE_INSTALL_DIR)/include" ] || cp -r include $(LIBEXAMPLE_INSTALL_DIR)
	cp libexample.a $(LIBEXAMPLE_INSTALL_DIR)/lib

clean:
	rm -rf *.o $(RESULT_TARGETS) lib*

.PHONY: clean install install-libexample $(LIBEXAMPLE_SONAME) $(LIBEXAMPLE_REALNAME)
