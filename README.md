# About

This project is written as an example for lesson regarding Linux shared and
static libraries.

# Building

The following command
- builds `libexample.so.1.0` shared library
- creates symlinks to the library: `libexample.so.1` and `libexample.so`
- builds `libexample.a` static library
- builds `test` executable to test the shared library
- builds `test-static` executable to test the static library

Libraries are installed into `lib` directory, headers - into `include` directory.

```
make && make install
```

# Running tests

## Shared library

The library is installed into the root of the project. So running an executable
compiled against the shared library needs `LD_LIBRARY_PATH` pointing to the
directory of the project root. We can just run `runtest.sh` shell script which
sets up `LD_LIBRARY_PATH` accordingly, e.g.:
```
$ ./runtest.sh example.c
File type:                regular file
I-node number:            1975450
Mode:                     100644 (octal)
Link count:               1
Ownership:                UID=1000   GID=1000
Preferred I/O block size: 4096 bytes
File size:                107 bytes
Blocks allocated:         8
Last status change:       Sat Feb 28 10:07:35 2015
Last file access:         Sat Feb 28 10:07:40 2015
Last file modification:   Sat Feb 28 10:07:35 2015
```

## Static library

Since static library is embedded into `test-static`, we don't need any external
files:
```
$ ./test-static example.c
File type:                regular file
I-node number:            1976307
Mode:                     100644 (octal)
Link count:               1
Ownership:                UID=1000   GID=1000
Preferred I/O block size: 4096 bytes
File size:                171 bytes
Blocks allocated:         8
Last status change:       Sat Feb 28 12:09:42 2015
Last file access:         Sat Feb 28 12:10:15 2015
Last file modification:   Sat Feb 28 12:09:42 2015
```
