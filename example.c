/* Copyright © 2015 - Ruslan Osmanov <rrosmanov@gmail.com> */

#include "example.h"

int e_stat(const char *pathname, struct stat *buf)
{
  return stat(pathname, buf);
}
