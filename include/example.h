/* Copyright © 2015 - Ruslan Osmanov <rrosmanov@gmail.com> */
#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int e_stat(const char *pathname, struct stat *buf);

#endif // EXAMPLE_H
