#!/bin/bash -
# LD_LIBRARY_PATH tells the system where to search for shared libraries,
# if the library is not installed in /lib, or /usr/lib.
export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH
exec ./test $@
